﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EventPlanning.Models.DbModels.Events;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.ViewModels.Events;
using EventPlanning.Models.ViewModels.Users;

namespace EventPlanning.Core.AutoMapper
{
    public sealed class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Event, EventViewModel>()
                .ForMember(dest => dest.EventFields, opt => opt.MapFrom(src => (Dictionary<string, string>)src.Fields))
                .ForMember(dest => dest.EventId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
                .ForMember(dest => dest.EventOwnerId, opt => opt.MapFrom(src => src.EventOwnerId))
                .ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.EventName))
                .ForMember(dest => dest.EventMembersCount, opt => opt.MapFrom(src => src.EventMembersCount))
                .ForMember(dest => dest.EventMembersIds, opt => opt.MapFrom(src => src.EventMembersIds))
                .ForMember(dest => dest.MembersLimit, opt => opt.MapFrom(src => src.MembersLimit));

            CreateMap<CreateEventViewModel, Event>()
               .ForMember(dest => dest.EventMembersCount, opt => opt.MapFrom(src => 0))
               .ForMember(dest => dest.EventMembersIds, opt => opt.MapFrom(src => new List<string>()))
               .ForMember(dest => dest.EventName, opt => opt.MapFrom(src => src.EventName))
               .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
               .ForMember(dest => dest.UsersInvitingToTheEvent, opt => opt.MapFrom(src => new List<string>()))
               .ForMember(dest => dest.UsersRejectedFromEvent, opt => opt.MapFrom(src => new List<string>()))
               .ForMember(dest => dest.UsersWaitingForTheConfirmation, opt => opt.MapFrom(src => new List<string>()))
               .ForMember(dest => dest.MembersLimit, opt => opt.MapFrom(src => src.MembersLimit));

            CreateMap<UserRegisterModel, User>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.Surname));            
        }
    }
}

﻿namespace EventPlanning.Core.Logger
{
    public interface ILogProvider
    {
        void Add(ILog logger);

        void LogError(string message);
    }
}

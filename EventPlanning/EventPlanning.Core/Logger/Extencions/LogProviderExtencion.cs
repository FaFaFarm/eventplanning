﻿namespace EventPlanning.Core.Logger.Extencions
{
    public static class LogProviderExtioncions
    {
        public static ILogProvider AddFileLogger(this ILogProvider logProvider, string path)
        {
            logProvider.Add(new FileLogger(path));
            return logProvider;
        }

        public static ILogProvider AddConsoleLogger(this ILogProvider logProvider)
        {
            logProvider.Add(new ConsoleLogger());
            return logProvider;
        }
    }
}

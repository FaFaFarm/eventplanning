﻿using System;
using System.Collections.Generic;

namespace EventPlanning.Core.Logger
{
    public class LogProvider : ILogProvider
    {
        private List<ILog> _loggers;

        public LogProvider()
        {
            _loggers = new List<ILog>();
        }

        public void Add(ILog logger)
        {
            if (logger == null)
            {
                throw new ArgumentNullException(nameof(logger));
            }

            _loggers.Add(logger);
        }

        public void LogError(string message)
        {
            foreach (var logger in _loggers)
            {
                logger.Log(message);
            }
        }
    }
}

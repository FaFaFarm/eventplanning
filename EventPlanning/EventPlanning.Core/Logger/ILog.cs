﻿namespace EventPlanning.Core.Logger
{
    public interface ILog
    {
        void Log(string message);
    }
}

﻿using System;

namespace EventPlanning.Core.Logger
{
    public sealed class ConsoleLogger : ILog
    {
        public void Log(string message)
        {
            Console.WriteLine(GetResultMessage(message));
        }

        private string GetResultMessage(string message)
        {
            return DateTime.Now + " : " + message;
        }
    }
}

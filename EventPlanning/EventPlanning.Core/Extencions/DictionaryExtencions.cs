﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventPlanning.Core.Extencions
{
    public static class DictionaryExtencions
    {
        public static object[] ToObjectArray<TKey, TValue>(this Dictionary<TKey, TValue> sourceDictionary)
        {
            if (sourceDictionary == null)
            {
                throw new NullReferenceException(nameof(sourceDictionary));
            }

            var distinationObjectList = new object[sourceDictionary.Count];

            return sourceDictionary.Select(x => distinationObjectList.Append(new { x.Key, x.Value })).ToArray();            
        }
    }
}

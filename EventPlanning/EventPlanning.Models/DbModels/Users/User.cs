﻿using Microsoft.AspNetCore.Identity;

namespace EventPlanning.Models.DbModels.Users
{
    public sealed class User : IdentityUser
    {
        public string Name { get; set; }

        public string Surname { get; set; }
    }
}

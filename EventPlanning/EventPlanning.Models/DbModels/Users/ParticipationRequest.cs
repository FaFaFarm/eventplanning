﻿namespace EventPlanning.Models.DbModels.Users
{
    public sealed class ParticipationRequest
    {
        public int Id { get; set; }

        public string EventParticipantId { get; set; }

        public string EventOwnerId { get; set; }

        public string EventId { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsRejected { get; set; }

        public string ConfirmationToken { get; set; }
    }
}

﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace EventPlanning.Models.DbModels.Events
{
    public sealed class Event
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string EventOwnerId { get; set; }

        public int EventMembersCount { get; set; }

        public List<string> EventMembersIds { get; set; }

        public string EventName { get; set; }

        public object Fields { get; set; }

        public string ImageUrl { get; set; }

        public List<string> UsersInvitingToTheEvent { get; set; }

        public List<string> UsersWaitingForTheConfirmation { get; set; }

        public List<string> UsersRejectedFromEvent { get; set; }

        public int MembersLimit { get; set; }
    }
}

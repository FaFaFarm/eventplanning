﻿using System.Collections.Generic;

namespace EventPlanning.Models
{
    public interface IOperationResult
    {
        public int StatusCode { get; }

        public List<string> Errors { get; }

        public bool IsSuccessful { get; }

        public void AddError(string error);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace EventPlanning.Models.Results
{
    public sealed class OperationDataResult<T> : IOperationResult
    {
        public OperationDataResult(T data, string error, int statusCode)
        {
            Data = data;
            Errors = new List<string>();
            Errors.Add(error);
            StatusCode = statusCode;
        }

        public OperationDataResult(T data, List<string> errors, int statusCode)
        {
            Data = data;
            Errors = new List<string>(errors);
            StatusCode = statusCode;
        }

        public OperationDataResult(T data)
        {
            Errors = new List<string>();
            Data = data;
            StatusCode = (int)HttpStatusCode.OK;
        }

        public T Data { get; private set; }

        public bool IsEmpty => Data == null;

        public int StatusCode { get; private set; }

        public List<string> Errors { get; private set; }

        public bool IsSuccessful => Errors == null || !Errors.Any() || StatusCode == (int)HttpStatusCode.OK;

        public void AddError(string error)
        {
            Errors.Add(error);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace EventPlanning.Models.Results
{
    public sealed class OperationResult : IOperationResult
    {
        public OperationResult(string error, int statusCode)
        {
            Errors = new List<string>();
            Errors.Add(error);
            StatusCode = statusCode;
        }

        public OperationResult(List<string> errors, int statusCode)
        {
            Errors = new List<string>(errors);
            StatusCode = statusCode;
        }

        public OperationResult()
        {
            StatusCode = (int)HttpStatusCode.OK;
        }

        public int StatusCode { get; private set; }

        public List<string> Errors { get; private set; }

        public bool IsSuccessful => Errors == null || !Errors.Any() || StatusCode == (int)HttpStatusCode.OK;

        public void AddError(string error)
        {
            if (Errors == null)
            {
                Errors = new List<string>();
            }

            Errors.Add(error);
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace EventPlanning.Models.ViewModels.Users
{
    public sealed class UserRegisterModel
    {
        [Display(Name = "Name")]
        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "NameCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Name { get; set; }

        [Display(Name = "Surname")]
        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "SurnameCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Surname { get; set; }

        [Display(Name = "Password")]
        [DataType(dataType: DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "PasswordCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceName = "PasswordsAreNotEquals", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress, ErrorMessageResourceName = "IncorrectEmailEntered", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "EmailCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Email { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace EventPlanning.Models.ViewModels.Users
{
    public sealed class UserAuthenticationModel
    {
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "EmailCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "PasswordCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string Password { get; set; }
    }
}

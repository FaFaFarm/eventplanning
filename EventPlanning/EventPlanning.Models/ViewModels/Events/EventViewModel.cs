﻿using System;
using System.Collections.Generic;

namespace EventPlanning.Models.ViewModels.Events
{
    public class EventViewModel : ICloneable
    {
        public string EventId { get; set; }

        public string EventName { get; set; }

        public string EventOwnerId { get; set; }

        public Dictionary<string, string> EventFields { get; set; }

        public string ImageUrl { get; set; }

        public int EventMembersCount { get; set; }

        public int MembersLimit { get; set; }

        public List<int> EventMembersIds { get; set; }

        public bool IsWaitForConfirmation { get; set; }

        public bool IsRejected { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsOwnedEvent { get; set; }

        public object Clone()
        {
            return new EventViewModel
            {
                EventFields = new Dictionary<string, string>(EventFields),
                EventId = EventId,
                EventName = EventName,
                EventOwnerId = EventOwnerId,
                ImageUrl = ImageUrl,
                EventMembersCount = EventMembersCount,
                EventMembersIds = new List<int>(EventMembersIds),
                IsAccepted = IsAccepted,
                IsOwnedEvent = IsOwnedEvent,
                IsRejected = IsRejected,
                IsWaitForConfirmation = IsWaitForConfirmation,
                MembersLimit = MembersLimit
            };
        }
    }
}

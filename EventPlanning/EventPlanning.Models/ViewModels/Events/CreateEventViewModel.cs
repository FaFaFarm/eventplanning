﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EventPlanning.Models.ViewModels.Events
{
    public sealed class CreateEventViewModel
    {
        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "EventTitleCannotBeEmpty", ErrorMessageResourceType = typeof(Resources.Global.Resources))]
        public string EventName { get; set; }

        public Dictionary<string, string> EventFields { get; set; }

        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false)]
        public string ImageUrl { get; set; }

        public int MembersLimit { get; set; }
    }
}

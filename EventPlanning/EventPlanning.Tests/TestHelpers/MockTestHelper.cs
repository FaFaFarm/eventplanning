﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Moq;

namespace EventPlanning.Tests.TestHelpers
{
    internal static class MockTestHelper
    {
        public static Mock<UserManager<TUser>> MockUserManager<TUser>(IList<TUser> ls) where TUser : class
        {
            var store = new Mock<IUserStore<TUser>>();
            var movkedManager = new Mock<UserManager<TUser>>(store.Object, null, null, null, null, null, null, null, null);
            movkedManager.Object.UserValidators.Add(new UserValidator<TUser>());
            movkedManager.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

            movkedManager.Setup(x => x.DeleteAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);
            movkedManager.Setup(x => x.CreateAsync(It.IsAny<TUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success).Callback<TUser, string>((x, y) => ls.Add(x));
            movkedManager.Setup(x => x.UpdateAsync(It.IsAny<TUser>())).ReturnsAsync(IdentityResult.Success);

            return movkedManager;
        }
    }
}

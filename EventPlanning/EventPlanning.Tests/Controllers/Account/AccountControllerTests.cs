﻿using EventPlanning.Controllers.Account;
using EventPlanning.Logic.MailLogic;
using EventPlanning.Logic.RegistrationLogic;
using EventPlanning.Models;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.Results;
using EventPlanning.Models.ViewModels.Users;
using EventPlanning.Tests.Fakes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;

namespace EventPlanning.Tests.Controllers.Account
{
    [TestFixture]
    internal sealed class AccountControllerTests
    {
        private Mock<IRegistrationLogic> _registrationLogicMock;

        private Mock<FakeUserManager<User>> _userManagerMock;
 
        private Mock<FakeSignInManager<User>> _signinManagerMock;
        
        private Mock<IMailLogic> _mailLogicMock;

        private Mock<IConfiguration> _configurationMock;

        private Mock<FakeHttpContext> _fakeHttpContext;

        private Mock<IUrlHelper> _urlHelperMock;

        private AccountController _accountController;

        [SetUp]
        public void SetUp()
        {           
            _userManagerMock = new Mock<FakeUserManager<User>>();

            _registrationLogicMock = new Mock<IRegistrationLogic>();

            _signinManagerMock = new Mock<FakeSignInManager<User>>();

            _mailLogicMock = new Mock<IMailLogic>();

            _configurationMock = new Mock<IConfiguration>();

            _urlHelperMock = new Mock<IUrlHelper>();

            _fakeHttpContext = new Mock<FakeHttpContext>();

            _accountController = new AccountController(
                _registrationLogicMock.Object,
                _userManagerMock.Object,
                _signinManagerMock.Object,
                _mailLogicMock.Object,
                _configurationMock.Object)
            {
                Url = _urlHelperMock.Object,
                ControllerContext = new ControllerContext()                
            };

            _accountController.ControllerContext.HttpContext = new DefaultHttpContext();
        }

        [Test]
        public void Login_ShouldCallFindByEmailAsync_OnceWithAnyArguments()
        {
            // Arrange
            var task = Task<User>.Factory.StartNew(() =>
            {
                return new User();
            });

            _userManagerMock.Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns(task);

            // Act
            _accountController.Login(new UserAuthenticationModel());

            task.Wait();

            // Assert                    
            _userManagerMock.Verify(x => x.FindByEmailAsync(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Login_ShouldCallFindByEmailAsync_OnceWithConcreteArguments()
        {
            // Arrange
            var task = Task<User>.Factory.StartNew(() =>
            {
                return new User();
            });

            var userAuthenticationModel = new UserAuthenticationModel
            {
                Email = "someEmail@gmail.com",
                Password = "1234567"
            };

            _userManagerMock.Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns(task);

            // Act
            _accountController.Login(userAuthenticationModel);

            task.Wait();

            // Assert                    
            _userManagerMock.Verify(x => x.FindByEmailAsync(It.Is<string>(y => y == userAuthenticationModel.Email)), Times.Once);
        }

        [Test]
        public void Login_ShouldCallIsEmailConfirmedAsync_OnceWithAnyArguments()
        {
            // Arrange
            var task = Task<bool>.Factory.StartNew(() =>
            {
                return true;
            });

            _userManagerMock.Setup(x => x.IsEmailConfirmedAsync(It.IsAny<User>()))
                .Returns(task);

            // Act
            _accountController.Login(new UserAuthenticationModel());

            task.Wait();

            // Assert                    
            _userManagerMock.Verify(x => x.IsEmailConfirmedAsync(It.IsAny<User>()), Times.Once);
        }

        [Test]
        public void Login_ShouldCallIsEmailConfirmedAsync_OnceWithConcreteArguments()
        {
            // Arrange
            var task = Task<bool>.Factory.StartNew(() =>
            {
                return true;
            });

            var user = new User
            {
                Email = "emil@gmail.com",
                EmailConfirmed = true,
                Id = "dddfffff",
                Surname = "Kostychenko",
                Name = "Lev"
            };

            var userTask = Task<User>.Factory.StartNew(() =>
            {
                return user;
            });

            _userManagerMock.Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns(userTask);

            _userManagerMock.Setup(x => x.IsEmailConfirmedAsync(It.IsAny<User>()))
                .Returns(task);

            // Act
            _accountController.Login(new UserAuthenticationModel());

            task.Wait();

            userTask.Wait();

            // Assert                    
            _userManagerMock.Verify(x => x.IsEmailConfirmedAsync(It.Is<User>(
                y => y.Name == user.Name &&
                y.Email == user.Email &&
                y.Surname == user.Surname &&
                y.Id == user.Id &&
                y.EmailConfirmed == user.EmailConfirmed)), Times.Once);
        }

        [Test]
        public void Login_ShouldCallPasswordSignInAsync_OnceWithAnyArguments()
        {
            // Arrange
            var signedInTask = Task<Microsoft.AspNetCore.Identity.SignInResult>.Factory.StartNew(() =>
            {
                return new Microsoft.AspNetCore.Identity.SignInResult();
            });

            var task = Task<bool>.Factory.StartNew(() =>
            {
                return true;
            });

            _userManagerMock.Setup(x => x.IsEmailConfirmedAsync(It.IsAny<User>()))
                .Returns(task);

            _signinManagerMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(signedInTask);

            // Act
            _accountController.Login(new UserAuthenticationModel());

            signedInTask.Wait();

            task.Wait();

            // Assert                    
            _signinManagerMock.Verify(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
        }

        [Test]
        public void Login_ShouldCallPasswordSignInAsync_OnceWithConcreteArguments()
        {
            // Arrange
            var signedInTask = Task<Microsoft.AspNetCore.Identity.SignInResult>.Factory.StartNew(() =>
            {
                return new Microsoft.AspNetCore.Identity.SignInResult();
            });

            var task = Task<bool>.Factory.StartNew(() =>
            {
                return true;
            });

            _userManagerMock.Setup(x => x.IsEmailConfirmedAsync(It.IsAny<User>()))
                .Returns(task);

            var model = new UserAuthenticationModel
            {
                Email = "TestEmail@gmail.com",
                Password = "testpass"
            };

            _signinManagerMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(signedInTask);           

            // Act
            _accountController.Login(model);

            task.Wait();

            signedInTask.Wait();

            // Assert                    
            _signinManagerMock.Verify(x => x.PasswordSignInAsync(
                It.Is<string>(y => y == model.Email), 
                It.Is<string>(y => y == model.Password), 
                It.Is<bool>(y => y), 
                It.Is<bool>(y => !y)), Times.Once);
        }

        [Test]
        public void Login_ShouldCallPasswordSignInAsync_NeverIfIsEmailConfirmedAsyncReturnsFalse()
        {
            // Arrange
            var signedInTask = Task<Microsoft.AspNetCore.Identity.SignInResult>.Factory.StartNew(() =>
            {
                return new Microsoft.AspNetCore.Identity.SignInResult();
            });

            var task = Task<bool>.Factory.StartNew(() =>
            {
                return false;
            });

            _userManagerMock.Setup(x => x.IsEmailConfirmedAsync(It.IsAny<User>()))
                .Returns(task);

            _signinManagerMock.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Returns(signedInTask);

            // Act
            _accountController.Login(new UserAuthenticationModel());

            signedInTask.Wait();

            task.Wait();

            // Assert                    
            _signinManagerMock.Verify(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Never);
        }

        [Test]
        public void Register_ShouldCallRegisterAsync_OnceWithAnyArguments()
        {
            // Arrange
            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _registrationLogicMock.Setup(x => x.RegisterAsync(It.IsAny<UserRegisterModel>()))
                .Returns(registerTask);

            // Act
            _accountController.Register(new UserRegisterModel());

            registerTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.RegisterAsync(It.IsAny<UserRegisterModel>()), Times.Once);
        }

        [Test]
        public void Register_ShouldCallRegisterAsync_OnceWithConcreteArguments()
        {
            // Arrange
            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _registrationLogicMock.Setup(x => x.RegisterAsync(It.IsAny<UserRegisterModel>()))
                .Returns(registerTask);

            var model = new UserRegisterModel
            {
                Email = "TestEmail@gmail.com",
                Password = "testpass",
                ConfirmPassword = "testpass",
                Name = "name",
                Surname = "surname"
            };

            // Act
            _accountController.Register(model);
            
            registerTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.RegisterAsync(It.Is<UserRegisterModel>( y => 
            y.Surname == model.Surname &&
            y.Password == model.Password &&
            y.Name == model.Name &&
            y.Email == model.Email &&
            y.ConfirmPassword == model.ConfirmPassword)), Times.Once);
        }

        [Test]
        public void Register_ShouldCallSendConfirmationMailAsync_OnceWithAnyArguments()
        {
            // Arrange
            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationDataResult<UserRegistrationConfiramtionModel>(
                    new UserRegistrationConfiramtionModel()
                    {
                        UserId = "id",
                        Token = "dawd34444fff"
                    });
            });

            var confirmationTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _accountController.ControllerContext.HttpContext.Request.Scheme = "default scheme";

            _urlHelperMock.Setup(x => x.Action(new UrlActionContext()))
                .Returns(It.IsAny<string>());

            _registrationLogicMock.Setup(x => x.RegisterAsync(It.IsAny<UserRegisterModel>()))
                .Returns(registerTask);

            _registrationLogicMock.Setup(x => x.SendConfirmationMailAsync(It.IsAny<UserRegistrationConfiramtionModel>(), It.IsAny<string>()))
                .Returns(confirmationTask);

            // Act
            _accountController.Register(new UserRegisterModel());

            registerTask.Wait();

            confirmationTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.SendConfirmationMailAsync(It.IsAny<UserRegistrationConfiramtionModel>(), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Register_ShouldCallSendConfirmationMailAsync_OnceWithConcreteArguments()
        {
            var confirmModel = new UserRegistrationConfiramtionModel
            {
                Email = "hello@ggmai.com",
                Name = "name",
                Surname = "Some Surname",
                Token = "token",
                UserId = "userid"
            };

            var result = new OperationDataResult<UserRegistrationConfiramtionModel>(confirmModel);

            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return result;
            });

            var confirmationTask = Task.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _registrationLogicMock.Setup(x => x.RegisterAsync(It.IsAny<UserRegisterModel>()))
                .Returns(registerTask);

            _registrationLogicMock.Setup(x => x.SendConfirmationMailAsync(It.IsAny<UserRegistrationConfiramtionModel>(), It.IsAny<string>()))
                .Returns(confirmationTask);

            _urlHelperMock.Setup(x => x.Action(new UrlActionContext()))
                .Returns(It.IsAny<string>());

            // Act
            _accountController.Register(new UserRegisterModel());

            registerTask.Wait();

            confirmationTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.SendConfirmationMailAsync(
                It.Is<UserRegistrationConfiramtionModel>(y =>
                    y.UserId == confirmModel.UserId &&
                    y.Token == confirmModel.Token &&
                    y.Name == confirmModel.Name &&
                    y.Surname == confirmModel.Surname &&
                    y.Email == confirmModel.Email), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void EmailConfirmation_ShouldCallConfirmEmailAsync_OnceWithAnyArguments()
        {
            // Arrange
            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _registrationLogicMock.Setup(x => x.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(registerTask);

            // Act
            _accountController.EmailConfirmation(It.IsAny<string>(), It.IsAny<string>());

            registerTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void EmailConfirmation_ShouldCallConfirmEmailAsync_OnceWithConcreteArguments()
        {
            // Arrange
            var registerTask = Task<IOperationResult>.Factory.StartNew(() =>
            {
                return new OperationResult();
            });

            _registrationLogicMock.Setup(x => x.ConfirmEmailAsync(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(registerTask);

            // Act
            _accountController.EmailConfirmation("223ff-gghj556-fd990", "someToken123");

            registerTask.Wait();

            // Assert                    
            _registrationLogicMock.Verify(x => x.ConfirmEmailAsync(
                It.Is<string>(x => x == "223ff-gghj556-fd990"), 
                It.Is<string>(x => x == "someToken123")), Times.Once);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventPlanning.DataLayer.Contexts;
using Microsoft.EntityFrameworkCore;

namespace EventPlanning.DataLayer.Repositories.Base
{
    public abstract class UsersBaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        private readonly UsersContext _usersContext;

        public UsersBaseRepository(UsersContext usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));

            _dbSet = usersContext.Set<TEntity>();
        }

        public virtual void Add(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            _dbSet.Add(record);
            _usersContext.SaveChanges();
        }

        public virtual async Task AddAsync(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            await _dbSet.AddAsync(record);
            await _usersContext.SaveChangesAsync();
        }

        public virtual void Edit(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            _dbSet.Update(record);
            _usersContext.SaveChanges();
        }

        public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public virtual TEntity GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        public virtual void Remove(TEntity record)
        {
            if (record == null)
            {
                throw new ArgumentNullException(nameof(record));
            }

            if (_usersContext.Entry(record).State == EntityState.Detached)
            {
                _dbSet.Attach(record);
            }

            _dbSet.Remove(record);
        }

        public virtual void Remove(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Remove(entityToDelete);
        }
    }
}

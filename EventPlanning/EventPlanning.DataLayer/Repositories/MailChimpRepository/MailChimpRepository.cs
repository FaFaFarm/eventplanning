﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailChimp.Net;
using MailChimp.Net.Models;
using Microsoft.Extensions.Configuration;

namespace EventPlanning.DataLayer.Repositories.MailChimpRepository
{
    public sealed class MailchimpRepository : IMailChimpRepository
    {
        private readonly IConfiguration _configuration;
        private readonly MailChimpManager _mailChimpManager;

        public MailchimpRepository(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));

            _mailChimpManager = new MailChimpManager(configuration["EmailConfiguration:MailchipAPIKey"]);
        }

        public IEnumerable<Template> GetAllTemplates()
        {
            return _mailChimpManager.Templates.GetAllAsync().Result;
        }

        public IEnumerable<List> GetAllMailingLists()
        {
            return _mailChimpManager.Lists.GetAllAsync().Result;
        }

        public async Task<object> GetTemplateDefaultContentAsync(string templateId)
        {
            return await _mailChimpManager.Templates.GetDefaultContentAsync(templateId);
        }

        public async Task<Template> GetTemplateByIdAsync(int templateId)
        {
            return await _mailChimpManager.Templates.GetAsync(templateId);
        }

        public Template GetTemplateByName(string templateName)
        {
            return _mailChimpManager.Templates.GetAllAsync().Result.FirstOrDefault(x => x.Name == templateName);
        }
    }
}
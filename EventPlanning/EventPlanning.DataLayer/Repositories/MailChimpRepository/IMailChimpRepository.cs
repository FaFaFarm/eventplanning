﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MailChimp.Net.Models;

namespace EventPlanning.DataLayer.Repositories.MailChimpRepository
{
    public interface IMailChimpRepository
    {
        IEnumerable<Template> GetAllTemplates();

        IEnumerable<List> GetAllMailingLists();

        Task<object> GetTemplateDefaultContentAsync(string tempateId);

        Task<Template> GetTemplateByIdAsync(int templateId);

        Template GetTemplateByName(string templateName);
    }
}

﻿using System;
using System.Collections.Generic;
using EventPlanning.DataLayer.Contexts;
using EventPlanning.DataLayer.Repositories.Base;
using EventPlanning.Models.DbModels.Users;

namespace EventPlanning.DataLayer.Repositories.ParticipationRequestsRepository
{
    public sealed class ParticipationRequestsRepository : UsersBaseRepository<ParticipationRequest>, IParticipationRequestsRepository
    {
        private readonly UsersContext _usersContext;

        public ParticipationRequestsRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

    }
}

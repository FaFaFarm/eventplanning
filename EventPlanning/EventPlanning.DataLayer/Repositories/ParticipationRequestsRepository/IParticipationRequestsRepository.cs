﻿using System.Collections.Generic;
using EventPlanning.Models.DbModels.Users;

namespace EventPlanning.DataLayer.Repositories.ParticipationRequestsRepository
{
    public interface IParticipationRequestsRepository : IRepository<ParticipationRequest>
    {
    }
}

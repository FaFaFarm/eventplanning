﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EventPlanning.DataLayer.Contexts;
using EventPlanning.DataLayer.Repositories.Base;
using EventPlanning.Models.DbModels.Users;
using Microsoft.EntityFrameworkCore;

namespace EventPlanning.DataLayer.Repositories.UsersRepository
{
    public sealed class UserRepository : UsersBaseRepository<User>, IUserRepository
    {
        private readonly UsersContext _usersContext;

        public UserRepository(UsersContext usersContext) : base(usersContext)
        {
            _usersContext = usersContext ?? throw new NullReferenceException(nameof(usersContext));
        }

        public async Task<List<User>> GetUsersByEmailAsync(string email)
        {
            return await _usersContext.Users.Where(x => x.Email == email).AsQueryable().ToListAsync();
        }
    }
}

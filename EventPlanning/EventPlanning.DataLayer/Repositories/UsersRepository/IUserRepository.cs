﻿using EventPlanning.Models.DbModels.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventPlanning.DataLayer.Repositories.UsersRepository
{
    public interface IUserRepository : IRepository<User>
    {
        Task<List<User>> GetUsersByEmailAsync(string email);
    }
}

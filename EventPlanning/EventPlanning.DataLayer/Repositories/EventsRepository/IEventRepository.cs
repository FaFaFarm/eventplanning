﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventPlanning.Models.DbModels.Events;

namespace EventPlanning.DataLayer.Repositories.EventsRepository
{
    public interface IEventRepository : IRepository<Event>
    {
        Task<IEnumerable<Event>> GetAsync(Expression<Func<Event, bool>> filter = null);

        Task EditAsync(Event record);

        Task RemoveAsync(object id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EventPlanning.DataLayer.Contexts;
using EventPlanning.Models.DbModels.Events;
using MongoDB.Bson;
using MongoDB.Driver;

namespace EventPlanning.DataLayer.Repositories.EventsRepository
{
    public class EventRepository : IEventRepository
    {
        private readonly EventContext _eventDbContext;

        public EventRepository(EventContext eventDbContext)
        {
            _eventDbContext = eventDbContext ?? throw new NullReferenceException(nameof(eventDbContext));
        }

        public void Add(Event record)
        {
            _eventDbContext.Events.InsertOne(record);
        }

        public async Task AddAsync(Event record)
        {
            await _eventDbContext.Events.InsertOneAsync(record);
        }

        public async Task EditAsync(Event record)
        {
            await _eventDbContext.Events.ReplaceOneAsync(new BsonDocument("_id", new ObjectId(record.Id)), record);
        }

        public void Edit(Event record)
        {
            _eventDbContext.Events.ReplaceOne(new BsonDocument("_id", new ObjectId(record.Id)), record);
        }

        public IEnumerable<Event> Get(Expression<Func<Event, bool>> filter = null, Func<IQueryable<Event>, IOrderedQueryable<Event>> orderBy = null)
        {
            if (filter != null)
            {
               return _eventDbContext.Events.Find(filter).ToList();
            }

            return _eventDbContext.Events.Find(x => x.Id != null).ToList();
        }

        public async Task<IEnumerable<Event>> GetAsync(Expression<Func<Event, bool>> filter = null)
        {
            if (filter != null)
            {
                return await _eventDbContext.Events.Find(filter).ToListAsync();
            }

            return await _eventDbContext.Events.Find(x => x.Id != null).ToListAsync();
        }

        public IEnumerable<Event> GetAll()
        {
            return _eventDbContext.Events.Find(x => x.Id != null).ToList();
        }

        public Event GetById(object id)
        {
            return _eventDbContext.Events.Find(x => x.Id == (String)id).FirstOrDefault();
        }

        public async Task<Event> GetByIdAsync(object id)
        {
            return await _eventDbContext.Events.Find(x => x.Id == (String)id).FirstOrDefaultAsync();
        }

        public void Remove(Event record)
        {
            _eventDbContext.Events.DeleteOne(new BsonDocument("_id", new ObjectId(record.Id)));
        }

        public void Remove(object id)
        {
            _eventDbContext.Events.DeleteOne(new BsonDocument("_id", new ObjectId((String)id)));
        }

        public async Task RemoveAsync(object id)
        {
            await _eventDbContext.Events.DeleteOneAsync(new BsonDocument("_id", new ObjectId((String)id)));
        }
    }
}

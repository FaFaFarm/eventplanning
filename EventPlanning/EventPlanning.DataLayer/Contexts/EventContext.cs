﻿using EventPlanning.Models.DbModels.Events;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace EventPlanning.DataLayer.Contexts
{
    public sealed class EventContext
    {
        private readonly IMongoDatabase _mongoDatabase;
        private readonly IGridFSBucket _gridFSBucket;

        public EventContext(IConfiguration configuration)
        {
            string connectionString = configuration["ConnectionStrings:Events"];
            var connection = new MongoUrlBuilder(connectionString);
            var client = new MongoClient(connectionString);

            _mongoDatabase = client.GetDatabase(connection.DatabaseName);
            _gridFSBucket = new GridFSBucket(_mongoDatabase);
        }

        public IMongoCollection<Event> Events
        {
            get
            { 
                return _mongoDatabase.GetCollection<Event>("Events"); 
            }
        }
    }
}

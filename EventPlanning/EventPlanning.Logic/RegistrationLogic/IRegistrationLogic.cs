﻿using EventPlanning.Models;
using EventPlanning.Models.ViewModels.Users;
using System.Threading.Tasks;

namespace EventPlanning.Logic.RegistrationLogic
{
    public interface IRegistrationLogic
    {
        bool IsEmailTaken(string email);

        Task<IOperationResult> RegisterAsync(UserRegisterModel registerModel);

        Task SendConfirmationMailAsync(UserRegistrationConfiramtionModel confiramtionModel, string confirmationUrl);

        Task<IOperationResult> ConfirmEmailAsync(string userId, string token);
    }
}

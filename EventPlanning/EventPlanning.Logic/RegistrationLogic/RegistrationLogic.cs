﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EventPlanning.DataLayer.Repositories.UsersRepository;
using EventPlanning.Models;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.ViewModels.Users;
using EventPlanning.Models.Results;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using EventPlanning.Logic.MailLogic;
using Microsoft.Extensions.Configuration;

namespace EventPlanning.Logic.RegistrationLogic
{
    public sealed class RegistrationLogic : IRegistrationLogic
    {
        private readonly IUserRepository _userRepository;

        private readonly UserManager<User> _userManager;

        private readonly IMapper _mapper;

        private readonly IMailLogic _mailLogic;

        private readonly IConfiguration _configuration;

        public RegistrationLogic(
            IUserRepository userRepository,
            UserManager<User> userManager,
            IMapper mapper,
            IMailLogic mailLogic,
            IConfiguration configuration)
        {
            _userRepository = userRepository ?? throw new NullReferenceException(nameof(userRepository));

            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));

            _mapper = mapper ?? throw new NullReferenceException(nameof(mapper));

            _mailLogic = mailLogic ?? throw new NullReferenceException(nameof(mailLogic));

            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));
        }

        public bool IsEmailTaken(string email)
        {
            return _userRepository.GetUsersByEmailAsync(email).Result.Any();
        }

        public async Task<IOperationResult> RegisterAsync(UserRegisterModel registerModel)
        {
            if (IsEmailTaken(registerModel.Email))
            {
                return new OperationResult(Resources.Global.Resources.ThisEmailIsAlreadyTaken, (int)HttpStatusCode.NotFound);
            }

            var user = _mapper.Map<User>(registerModel);

            var identityResult = await _userManager.CreateAsync(user, registerModel.Password);

            if (!identityResult.Succeeded)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            var confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            var confiramtionModel = new UserRegistrationConfiramtionModel
            {
                Email = user.Email,
                Name = user.Name,
                Surname = user.Surname,
                Token = confirmationToken,
                UserId = user.Id
            };

            return new OperationDataResult<UserRegistrationConfiramtionModel>(confiramtionModel);
        }

        public async Task SendConfirmationMailAsync(UserRegistrationConfiramtionModel confiramtionModel, string confirmationUrl)
        {
            await _mailLogic.SendEmailAsync(
                _configuration["EmailConfiguration:CoreAdminEmail"],
                confiramtionModel.Email,
                Resources.Global.Resources.EmailConfirmation,
                _mailLogic.CreateEmailBody(_configuration["EmailConfiguration:EmailTemplatesDir"] + "RegistrationConfirmation.html", $"{confiramtionModel.Name} {confiramtionModel.Surname}", confirmationUrl));
        }

        public async Task<IOperationResult> ConfirmEmailAsync(string userId, string token)
        {
            if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(token))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            var result = await _userManager.ConfirmEmailAsync(user, token);

            if (!result.Succeeded)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            return new OperationResult();
        }
    }
}

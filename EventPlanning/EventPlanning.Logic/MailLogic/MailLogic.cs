﻿using System;
using System.IO;
using System.Threading.Tasks;
using EventPlanning.DataLayer.Repositories.MailChimpRepository;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace EventPlanning.Logic.MailLogic
{
    public sealed class MailLogic : IMailLogic
    {
        private readonly IConfiguration _configuration;

        public MailLogic(IConfiguration configuration, IMailChimpRepository mailChimpRepository)
        {
            _configuration = configuration ?? throw new NullReferenceException(nameof(mailChimpRepository));
        }

        public async Task SendEmailAsync(string emailFrom, string emailTo, string subject, string message)
        {
            IsEmailValid(emailFrom, emailTo, message);

            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(emailFrom));
            emailMessage.To.Add(new MailboxAddress(emailTo));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_configuration["EmailConfiguration:SMTPProvider"], 25, false);
                await client.AuthenticateAsync(_configuration["EmailConfiguration:CoreAdminEmail"], _configuration["EmailConfiguration:CoreAdminPassowrd"]);
                try
                {
                    await client.SendAsync(emailMessage);
                }
                finally
                {
                    await client.DisconnectAsync(true);
                }
            }
        }

        public string CreateEmailBody(string pathToHtml, params string[] args)
        {
            string htmlContent;

            using (var streamReader = new StreamReader(pathToHtml))
            {
                htmlContent = streamReader.ReadToEnd();
            }

            for (int index = 0; index < args.Length; index ++)
            {
                htmlContent = htmlContent.Replace("{"+index.ToString()+"}", args[index]);
            }

            return htmlContent;
        }

        private void IsEmailValid(string emailFrom, string emailTo, string message)
        {
            if (String.IsNullOrEmpty(emailFrom))
            {
                throw new ArgumentNullException(nameof(emailFrom) + Resources.Global.Resources.CannotBeEmpty);
            }
            else if (String.IsNullOrEmpty(emailTo))
            {
                throw new ArgumentNullException(nameof(emailTo) + Resources.Global.Resources.CannotBeEmpty);
            }
            else if (String.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException(nameof(message) + Resources.Global.Resources.CannotBeEmpty);
            }
        }
    }
}

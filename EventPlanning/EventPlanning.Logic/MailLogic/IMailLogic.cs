﻿using System.Threading.Tasks;

namespace EventPlanning.Logic.MailLogic
{
    public interface IMailLogic
    {
        Task SendEmailAsync(string emailFrom, string emailTo, string subject, string message);

        string CreateEmailBody(string pathToHtml, params string[] args);
    }
}

﻿using System;
using System.Security.Cryptography;

namespace EventPlanning.Logic.HashingLogic
{
    public class HashingLogic : IHashingLogic
    {
        public bool CompareWithRfc2898Hash(string stringToCompare, string hashCode)
        {
            byte[] hashBytes = Convert.FromBase64String(hashCode);

            var salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);

            var pbkdf2 = new Rfc2898DeriveBytes(stringToCompare, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            for (int i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                {
                    return false;
                }
            }

            return true;
        }

        public string CreateRfc2898HashPassword(string password)
        {
            var salt = new byte[16];

            new RNGCryptoServiceProvider().GetBytes(salt);

            var byteCode = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = byteCode.GetBytes(20);

            var hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            return Convert.ToBase64String(hashBytes);
        }
    }
}

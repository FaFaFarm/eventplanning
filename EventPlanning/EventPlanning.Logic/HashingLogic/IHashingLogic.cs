﻿namespace EventPlanning.Logic.HashingLogic
{
    public interface IHashingLogic
    {
        string CreateRfc2898HashPassword(string password);

        bool CompareWithRfc2898Hash(string stringToCompare, string hashCode);
    }
}

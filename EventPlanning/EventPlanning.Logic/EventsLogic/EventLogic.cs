﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EventPlanning.DataLayer.Repositories.EventsRepository;
using EventPlanning.DataLayer.Repositories.ParticipationRequestsRepository;
using EventPlanning.Logic.HashingLogic;
using EventPlanning.Logic.MailLogic;
using EventPlanning.Models;
using EventPlanning.Models.DbModels.Events;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.Results;
using EventPlanning.Models.ViewModels.Events;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace EventPlanning.Logic.EventsLogic
{
    public sealed class EventLogic : IEventLogic
    {
        private readonly IEventRepository _eventRepository;

        private readonly IMapper _mapper;

        private readonly IMailLogic _mailLogic;

        private readonly UserManager<User> _userManager;

        private readonly IConfiguration _configuration;

        private readonly IParticipationRequestsRepository _participationRequestsRepository;

        private readonly IHashingLogic _hashingLogic;

        public EventLogic(
            IEventRepository eventRepository, 
            IMapper mapper, 
            IMailLogic mailLogic, 
            UserManager<User> userManager, 
            IConfiguration configuration,
            IParticipationRequestsRepository participationRequestsRepository,
            IHashingLogic hashingLogic)
        {
            _eventRepository = eventRepository ?? throw new NullReferenceException(nameof(eventRepository));

            _mapper = mapper ?? throw new NullReferenceException(nameof(mapper));

            _mailLogic = mailLogic ?? throw new NullReferenceException(nameof(mailLogic));

            _userManager = userManager ?? throw new NullReferenceException(nameof(userManager));

            _configuration = configuration ?? throw new NullReferenceException(nameof(configuration));

            _participationRequestsRepository = participationRequestsRepository ?? throw new NullReferenceException(nameof(participationRequestsRepository));

            _hashingLogic = hashingLogic ?? throw new NullReferenceException(nameof(hashingLogic));
        }

        public async Task<IOperationResult> CreateNewEventAsync(CreateEventViewModel createEventModel, string eventOwnerId)
        {
            if (createEventModel == null || String.IsNullOrEmpty(eventOwnerId))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            if (createEventModel.MembersLimit <= 0)
            {
                return new OperationResult(Resources.Global.Resources.NumberOfParticipantsShouldBeMoreThenZero, (int)HttpStatusCode.ExpectationFailed);
            }

            var eventCustomFields = (Object)createEventModel.EventFields;

            var eventToCreate = _mapper.Map<Event>(createEventModel);
            eventToCreate.Fields = eventCustomFields;
            eventToCreate.EventOwnerId = eventOwnerId;

            try
            {
                await _eventRepository.AddAsync(eventToCreate);
            }
            catch
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            return new OperationDataResult<string>(eventToCreate.Id);
        }

        public IEnumerable<EventViewModel> GetAllEvents(string userId = null)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return _eventRepository.GetAll().Select(
                    x => _mapper.Map<EventViewModel>(x)).OrderBy(x => x.EventMembersCount);
            }

            IEnumerable<EventViewModel> viewModels = _eventRepository.GetAll().Select(
                    x => _mapper.Map<EventViewModel>(x)).OrderBy(x => x.EventMembersCount);

            IEnumerable<ParticipationRequest> participationRequest = _participationRequestsRepository.Get(x => x.EventParticipantId == userId || x.EventOwnerId == userId);

            if (participationRequest == null)
            {
                return viewModels;
            }

            var newViewModels = new List<EventViewModel>();

            foreach (var model in viewModels)
            {
                var viewModel = (EventViewModel)model.Clone();

                if (participationRequest.Any(x => model.EventId == x.EventId))
                {
                    ParticipationRequest currentParticipation = participationRequest.FirstOrDefault(x => x.EventId == model.EventId && (x.EventOwnerId == userId || x.EventParticipantId == userId));

                    if (currentParticipation != null)
                    {                       
                        viewModel.IsAccepted = currentParticipation.IsAccepted;
                        viewModel.IsRejected = currentParticipation.IsRejected;
                        viewModel.IsWaitForConfirmation = !(currentParticipation.IsRejected || currentParticipation.IsAccepted);
                        viewModel.IsOwnedEvent = currentParticipation.EventOwnerId == userId;
                    }
                }
                else
                {
                    if (model.EventOwnerId == userId)
                    {
                        viewModel.IsOwnedEvent = true;
                    }
                }

                newViewModels.Add(viewModel);
            }

            return newViewModels;            
        }

        public EventViewModel GetEventById(string eventId, string userId = null)
        {
            if (String.IsNullOrEmpty(userId))
            {
                return _mapper.Map<EventViewModel>(_eventRepository.GetById(eventId));
            }

            EventViewModel viewModel = _mapper.Map<EventViewModel>(_eventRepository.GetById(eventId));

            ParticipationRequest participationRequest = _participationRequestsRepository.Get(x => x.EventId == eventId && (x.EventParticipantId == userId || x.EventOwnerId == userId)).FirstOrDefault();

            if (participationRequest == null)
            {
                return viewModel;
            }

            viewModel.IsAccepted = participationRequest.IsAccepted;
            viewModel.IsRejected = participationRequest.IsRejected;
            viewModel.IsWaitForConfirmation = !(participationRequest.IsRejected || participationRequest.IsAccepted);
            viewModel.IsOwnedEvent = participationRequest.EventOwnerId == userId;

            return viewModel;
        }

        public async Task<IOperationResult> SubscribeToEventAsync(string eventId, string userId, string urlPart)
        {
            if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(eventId))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            Event oldEvent = await _eventRepository.GetByIdAsync(eventId);

            if (oldEvent == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            if (oldEvent.EventOwnerId == userId)
            {
                return new OperationResult(Resources.Global.Resources.CannotSubscribeToYourEvent, (int)HttpStatusCode.Conflict);
            }

            if (oldEvent.EventMembersCount >= oldEvent.MembersLimit)
            {
                return new OperationResult(Resources.Global.Resources.NumberOfParticipantsExceeded, (int)HttpStatusCode.ExpectationFailed);
            }

            string eventOwnerId = oldEvent.EventOwnerId;

            User subscriber = await _userManager.FindByIdAsync(userId);
            User eventOwner = await _userManager.FindByIdAsync(eventOwnerId);

            if (subscriber == null || eventOwner == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            string acceptanceUrl, rejectUrl, token;

            oldEvent.UsersWaitingForTheConfirmation.Add(userId);

            CreateUrlWithToken(urlPart, oldEvent.EventOwnerId, userId, eventId, out acceptanceUrl, out rejectUrl, out token);

            var participationRequest = new ParticipationRequest
            {
                ConfirmationToken = token,
                EventId = oldEvent.Id,
                EventOwnerId = eventOwnerId,
                EventParticipantId = userId,
                IsAccepted = false,
                IsRejected = false
            };

            try
            {
                await _participationRequestsRepository.AddAsync(participationRequest);

                await NotifyUserAsync(
                    _mailLogic.CreateEmailBody(
                        _configuration["EmailConfiguration:EmailTemplatesDir"] + "RequestToEventParticipation.html",
                        $"{eventOwner.Name} {eventOwner.Surname}",
                        $"{subscriber.Name} {subscriber.Surname}",
                        subscriber.Email,
                        oldEvent.EventName,
                        acceptanceUrl,
                        rejectUrl),
                    eventOwner.Email);

                await _eventRepository.EditAsync(oldEvent);
            }
            catch (Exception ex)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError + ex.Message, (int)HttpStatusCode.InternalServerError);
            }

            return new OperationResult();
        }

        public async Task<IOperationResult> RejectUserFromEventAsync(string eventId, string participantId, string eventOwnerId, string token)
        {
            if (String.IsNullOrEmpty(participantId) || String.IsNullOrEmpty(eventId))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            Event oldEvent = await _eventRepository.GetByIdAsync(eventId);

            if (oldEvent == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            ParticipationRequest participationRequest = _participationRequestsRepository.Get(
                x => !x.IsAccepted
                && !x.IsRejected
                && x.ConfirmationToken == token
                && x.EventParticipantId == participantId
                && x.EventOwnerId == eventOwnerId).FirstOrDefault();

            if (participationRequest == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            if (participationRequest.IsRejected || participationRequest.IsAccepted)
            {
                return new OperationResult(Resources.Global.Resources.ThisEventAlreadyRejectedOrAcceped, (int)HttpStatusCode.Conflict);
            }        

            participationRequest.IsRejected = true;

            _participationRequestsRepository.Edit(participationRequest);

            oldEvent.UsersWaitingForTheConfirmation.Remove(participantId);
            oldEvent.UsersRejectedFromEvent.Add(participantId);

            try
            {
                await _eventRepository.EditAsync(oldEvent);
            }
            catch (Exception ex)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError + ex.Message, (int)HttpStatusCode.InternalServerError);
            }

            User user = await _userManager.FindByIdAsync(participantId);

            if (user == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            await NotifyUserAsync(
                _mailLogic.CreateEmailBody(_configuration["EmailConfiguration:EmailTemplatesDir"] + "EventRejectNotification.html", $"{user.Name} {user.Surname}", oldEvent.EventName),
                user.Email);

            return new OperationResult();
        }

        public async Task<IOperationResult> AcceptUserSubscriptionAsync(string eventId, string participantId, string eventOwnerId, string token)
        {
            if (String.IsNullOrEmpty(eventOwnerId) || String.IsNullOrEmpty(eventId))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            Event oldEvent = await _eventRepository.GetByIdAsync(eventId);

            if (oldEvent == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }
            
            ParticipationRequest participationRequest = _participationRequestsRepository.Get(
                x => !x.IsAccepted 
                && !x.IsRejected 
                && x.ConfirmationToken == token 
                && x.EventParticipantId == participantId 
                && x.EventOwnerId == eventOwnerId).FirstOrDefault();

            if (participationRequest == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            if (participationRequest.IsRejected || participationRequest.IsAccepted)
            {
                return new OperationResult(Resources.Global.Resources.ThisEventAlreadyRejectedOrAcceped, (int)HttpStatusCode.Conflict);
            }

            participationRequest.IsAccepted = true;

            _participationRequestsRepository.Edit(participationRequest);

            oldEvent.UsersWaitingForTheConfirmation.Remove(participantId);
            oldEvent.UsersInvitingToTheEvent.Add(participantId);
            oldEvent.EventMembersCount ++;

            try
            {
                await _eventRepository.EditAsync(oldEvent);
            }
            catch (Exception ex)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError + ex.Message, (int)HttpStatusCode.InternalServerError);
            }

            User user = await _userManager.FindByIdAsync(participantId);

            if (user == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            await NotifyUserAsync(
                _mailLogic.CreateEmailBody(_configuration["EmailConfiguration:EmailTemplatesDir"] + "EventAcceptNotification.html", $"{user.Name} {user.Surname}", oldEvent.EventName),
                user.Email);

            return new OperationResult();
        }

        public async Task<IOperationResult> RemoveEvent(string userId, string eventId)
        {
            if (String.IsNullOrEmpty(eventId) || String.IsNullOrEmpty(userId))
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }

            User user = await _userManager.FindByIdAsync(userId);
            Event oldEvent = await _eventRepository.GetByIdAsync(eventId);

            if (oldEvent == null || user == null)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
            }                        
            
            if (oldEvent.EventOwnerId != user.Id)
            {
                return new OperationResult(Resources.Global.Resources.YouCannotRemoveThisEvent, (int)HttpStatusCode.BadRequest);
            }

            try
            {
                await _eventRepository.RemoveAsync(oldEvent.Id);
            }
            catch (Exception ex)
            {
                return new OperationResult(Resources.Global.Resources.UnexpectedServerError + ex.Message, (int)HttpStatusCode.InternalServerError);
            }            
            
            return new OperationResult();
        }

        private async Task NotifyUserAsync(string htmlTemplate, string userEmail)
        {
            await _mailLogic.SendEmailAsync(
                _configuration["EmailConfiguration:CoreAdminEmail"],
                userEmail,
                Resources.Global.Resources.EventNotification,
                htmlTemplate);
        }

        private void CreateUrlWithToken(string urlPart, string eventOwnerId, string participantId, string eventId, out string acceptanceUrl, out string rejectUrl, out string token)
        {
            var acceptanceUrlWithToken = new StringBuilder(urlPart);
            var rejectUrlWithToken = new StringBuilder(urlPart);
            token = CreateUniqueToken();

            acceptanceUrlWithToken.Append("?isAccept=true&");
            rejectUrlWithToken.Append("?isAccept=false&");

            acceptanceUrlWithToken.Append($"ownerId={eventOwnerId}&");
            rejectUrlWithToken.Append($"ownerId={eventOwnerId}&");

            acceptanceUrlWithToken.Append($"participantId={participantId}&");
            rejectUrlWithToken.Append($"participantId={participantId}&");

            acceptanceUrlWithToken.Append($"eventId={eventId}&");
            rejectUrlWithToken.Append($"eventId={eventId}&");

            acceptanceUrlWithToken.Append($"token={token}");
            rejectUrlWithToken.Append($"token={token}");

            rejectUrl = rejectUrlWithToken.ToString();
            acceptanceUrl = acceptanceUrlWithToken.ToString();
        }

        private string CreateUniqueToken()
        {
            return _hashingLogic.CreateRfc2898HashPassword(Guid.NewGuid().ToString()).Replace(' ', '-').Replace('+', '-').Replace('?', '-').Replace('&', '-');
        }
    }
}
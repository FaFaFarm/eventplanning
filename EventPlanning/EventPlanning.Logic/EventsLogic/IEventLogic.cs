﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventPlanning.Models;
using EventPlanning.Models.ViewModels.Events;

namespace EventPlanning.Logic.EventsLogic
{
    public interface IEventLogic
    {
        Task<IOperationResult> CreateNewEventAsync(CreateEventViewModel createEventModel, string eventOwnerId);

        IEnumerable<EventViewModel> GetAllEvents(string userId = null);

        EventViewModel GetEventById(string eventId, string userId = null);

        Task<IOperationResult> SubscribeToEventAsync(string eventId, string userId, string urlPart);

        Task<IOperationResult> RejectUserFromEventAsync(string eventId, string participantId, string eventOwnerId, string token);

        Task<IOperationResult> AcceptUserSubscriptionAsync(string eventId, string participantId, string eventOwnerId, string token);

        Task<IOperationResult> RemoveEvent(string userId, string eventId);
    }
}

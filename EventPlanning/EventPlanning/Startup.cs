using EventPlanning.DataLayer.Contexts;
using EventPlanning.DataLayer.Repositories.UsersRepository;
using EventPlanning.Logic.RegistrationLogic;
using FarmHouse.Api.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using EventPlanning.Core.Logger;
using EventPlanning.DataLayer.Repositories.EventsRepository;
using EventPlanning.Logic.EventsLogic;
using AutoMapper;
using EventPlanning.Core.AutoMapper;
using EventPlanning.Logic.MailLogic;
using EventPlanning.Models.DbModels.Users;
using Microsoft.AspNetCore.Identity;
using EventPlanning.DataLayer.Repositories.MailChimpRepository;
using EventPlanning.DataLayer.Repositories.ParticipationRequestsRepository;
using EventPlanning.Logic.HashingLogic;

namespace EventPlanning
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            #region logics

            services.AddScoped<IRegistrationLogic, RegistrationLogic>();
            services.AddScoped<IEventLogic, EventLogic>();
            services.AddScoped<IMailLogic, MailLogic>();
            services.AddScoped<IHashingLogic, HashingLogic>();

            #endregion

            #region repositories

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IEventRepository, EventRepository>();
            services.AddScoped<IMailChimpRepository, MailchimpRepository>();
            services.AddScoped<IParticipationRequestsRepository, ParticipationRequestsRepository>();

            #endregion

            services.AddDbContext<UsersContext>(op => op.UseSqlServer(Configuration["ConnectionStrings:UsersDB"]));
            services.AddScoped<EventContext>();

            services.AddSingleton<ILogProvider, LogProvider>();

            services.AddIdentity<User, IdentityRole>(op =>
            {
                op.Tokens.PasswordResetTokenProvider = TokenOptions.DefaultEmailProvider;
                op.Password.RequiredLength = 5;
                op.Password.RequireDigit = false;
                op.Password.RequireLowercase = false;
                op.Password.RequireUppercase = false;
                op.Password.RequireNonAlphanumeric = false;
                op.User.RequireUniqueEmail = false;                
                op.User.AllowedUserNameCharacters = Configuration["Identity:AlloweddUserNameSymbols"];
            })
            .AddEntityFrameworkStores<UsersContext>()
            .AddDefaultTokenProviders();
            
            #region mapper

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseMiddleware<LogMiddleware>();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();    
            app.UseAuthorization();     

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EventPlanning.Controllers
{
    [Authorize]
    public sealed class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult InfoPage(string infoText)
        {
            var errors = new List<string>
            {
                infoText
            };

            return View("InfoPage", errors);
        }
    }
}

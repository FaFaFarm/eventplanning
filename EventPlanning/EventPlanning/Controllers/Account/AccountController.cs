﻿using System;
using System.Threading.Tasks;
using EventPlanning.Logic.MailLogic;
using EventPlanning.Logic.RegistrationLogic;
using EventPlanning.Models;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.Results;
using EventPlanning.Models.ViewModels.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EventPlanning.Controllers.Account
{
    public class AccountController : Controller
    {
        private readonly IRegistrationLogic _registrationLogic;

        private readonly UserManager<User> _userManager;

        private readonly SignInManager<User> _signInManager;

        private readonly IMailLogic _mailLogic;

        private readonly IConfiguration _configuration;

        public AccountController(
            IRegistrationLogic registrationLogic, 
            UserManager<User> userManager, 
            SignInManager<User> signInManager,
            IMailLogic mailLogic,
            IConfiguration configuration)
        {
            _registrationLogic = registrationLogic ?? throw new ArgumentNullException(nameof(registrationLogic));

            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));

            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));

            _mailLogic = mailLogic ?? throw new ArgumentNullException(nameof(mailLogic));

            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserAuthenticationModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);

                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    ViewData["Email"] = user.Email;
                    return View("EmailConfirmation");
                }

                var authenticationResult = await _signInManager.PasswordSignInAsync(model.Email, model.Password, true, false);

                if (authenticationResult.Succeeded)
                {
                    ViewData["UserName"] = model.Email;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", Resources.Global.Resources.LoginOrEmailIsNotValid);
                }
            }            

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(UserRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                IOperationResult registerResult = await _registrationLogic.RegisterAsync(model);

                if (registerResult.IsSuccessful)
                {
                    var userRegistrationConfiramtion = ((OperationDataResult<UserRegistrationConfiramtionModel>)registerResult).Data;

                    var confirmationUrl = Url.Action(
                        "EmailConfirmation",
                        "Account",
                        new { userId = userRegistrationConfiramtion.UserId, token = userRegistrationConfiramtion.Token },
                        protocol: HttpContext.Request.Scheme);

                    await _registrationLogic.SendConfirmationMailAsync(userRegistrationConfiramtion, confirmationUrl);

                    ViewData["Email"] = userRegistrationConfiramtion.Email;
                    return View("EmailConfirmation");
                }
                else
                {
                    foreach (var error in registerResult.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirmation(string userId, string token)
        {
            IOperationResult result = await _registrationLogic.ConfirmEmailAsync(userId, token);

            if (result.IsSuccessful)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View("InfoPage", result.Errors);
            }
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }
    }
}
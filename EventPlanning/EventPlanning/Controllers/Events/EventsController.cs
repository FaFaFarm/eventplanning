﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EventPlanning.Logic.EventsLogic;
using EventPlanning.Models;
using EventPlanning.Models.DbModels.Users;
using EventPlanning.Models.Results;
using EventPlanning.Models.ViewModels.Events;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace EventPlanning.Controllers.Events
{
    [Authorize]
    public class EventsController : Controller
    {
        private readonly IEventLogic _eventLogic;

        private readonly UserManager<User> _userManager;

        public EventsController(IEventLogic eventLogic, UserManager<User> userManager) 
        {
            _eventLogic = eventLogic ?? throw new NullReferenceException(nameof(eventLogic));

            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager)); 
        }

        [HttpGet]
        public IActionResult CreateEvent()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AllEvents()
        {           
            var events = _eventLogic.GetAllEvents(_userManager.GetUserId(HttpContext.User)).ToList();

            return View(events);
        }

        [HttpGet]
        public IActionResult Event(string eventId)
        {
            return View(_eventLogic.GetEventById(eventId, _userManager.GetUserId(HttpContext.User)));
        }

        [HttpDelete]
        public async Task<IOperationResult> RemoveEvent([FromBody] string eventId)
        {
            await _eventLogic.RemoveEvent(_userManager.GetUserId(HttpContext.User), eventId);

            return new OperationDataResult<string>(Url.Action("AllEvents", "Events", null, this.Url.ActionContext.HttpContext.Request.Scheme));            
        }

        [HttpPost]
        public async Task<IOperationResult> CreateEvent([FromBody] CreateEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                IOperationResult operationResult = await _eventLogic.CreateNewEventAsync(model, _userManager.GetUserId(HttpContext.User));

                if (!operationResult.IsSuccessful)
                {
                    return operationResult;
                }

                string eventUrl = Url.Action("Event", "Events", new { eventId = ((OperationDataResult<string>)operationResult).Data}, this.Url.ActionContext.HttpContext.Request.Scheme);

                return new OperationDataResult<string>(eventUrl);
            }

            return new OperationResult(Resources.Global.Resources.UnexpectedServerError, (int)HttpStatusCode.InternalServerError);
        }

        [HttpPost]
        public async Task<IOperationResult> SubscribeToEvent([FromBody] string eventId)
        {
            string userId = _userManager.GetUserId(HttpContext.User);
            string redirectUrl = String.Empty;

            IOperationResult result = await _eventLogic.SubscribeToEventAsync(eventId, userId, Url.Action("ParticipationConfirmation", "Events", null, this.Url.ActionContext.HttpContext.Request.Scheme));

            if (result.IsSuccessful)
            {
                redirectUrl = Url.Action("Event", "Events", new { eventId = eventId }, this.Url.ActionContext.HttpContext.Request.Scheme);                
            }
            else
            {
                redirectUrl = Url.Action("InfoPage", "Home", new { infoText = result.Errors.FirstOrDefault() }, this.Url.ActionContext.HttpContext.Request.Scheme);
            }

            return new OperationDataResult<string>(redirectUrl);
        }

        [HttpGet]
        public async Task<IActionResult> ParticipationConfirmation(bool isAccept, string ownerId, string participantId, string eventId, string token)
        {
            IOperationResult operationResult;

            if (isAccept)
            {
                operationResult = await _eventLogic.AcceptUserSubscriptionAsync(eventId, participantId, ownerId, token);
            }
            else
            {
                operationResult = await _eventLogic.RejectUserFromEventAsync(eventId, participantId, ownerId, token);
            }

            if (operationResult.IsSuccessful)
            {
                return View("InfoPage", new List<string> { isAccept ? Resources.Global.Resources.RequestToEventAcceptSuccessfully : Resources.Global.Resources.RequestToEventRejsectSuccessfully });
            }
            else
            {
                return View("InfoPage", operationResult.Errors);
            }
        }
    }
}